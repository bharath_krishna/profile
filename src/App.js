import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import ProfilePage from "./components/homePage";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Route exact path="/" component={ProfilePage} />
      </BrowserRouter>
    );
  }
}

export default App;
