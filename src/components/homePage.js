import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import HeadPlate from "./headPlate";
import TabbedBody from "./tabbedBody";
// import Footer from "./footer";
import LeftPanel from "./leftPanel";

class ProfilePage extends Component {
  state = {};
  render() {
    return (
      <Container fluid>
        <Row>
          <Col sm={9}>
            <HeadPlate />
            <TabbedBody />
            {/* <Footer /> */}
          </Col>
          <Col sm={3}>
            <LeftPanel />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default ProfilePage;
