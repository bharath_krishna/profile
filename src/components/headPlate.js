import React, { Component } from "react";
import { Row, Col, Card } from "react-bootstrap";

class HeadPlate extends Component {
  state = {};
  render() {
    return (
      <Card bg="secondary" text="white" className="m-2">
        <Card.Body>
          <Row>
            <Col>
              <Card.Title>
                <h2>Bharath Krishna</h2>
              </Card.Title>
              <Card.Subtitle>Application Engineer</Card.Subtitle>
              <Card.Body>
                An Application Engineer with 8+ years of experience in software
                development using Python and PHP in IT field. Experience in
                leading complete software development cycle with 3-6 team
                members. Expertise in building microservice
                platform/orchestration using Object-Oriented python and its
                frameworks like Aiohttp, Tornado, Django, which is asynchronous
                and scalable with authentication and authorization gateway.
              </Card.Body>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    );
  }
}

export default HeadPlate;
