import React, { Component } from "react";
import { Tabs, Tab } from "react-bootstrap";
import Experience from "./experience";
import Education from "./education";

class TabbedBody extends Component {
  state = {};
  render() {
    return (
      <Tabs defaultActiveKey="exp" id="uncontrolled-tab-example">
        <Tab eventKey="exp" title="Experience">
          <Experience />
        </Tab>
        <Tab eventKey="edu" title="Education">
          <Education />
        </Tab>
      </Tabs>
    );
  }
}

export default TabbedBody;
