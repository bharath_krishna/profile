import React, { Component } from "react";
import { Badge, Row, Col, ListGroup, ProgressBar } from "react-bootstrap";
import { KeySkills } from "./skills";
import { IoLogoLinkedin, IoLogoGoogle } from "react-icons/io";
import { DiBitbucket } from "react-icons/di";

class PersonalInfo extends Component {
  state = {};
  render() {
    return (
      <ListGroup className="m-2">
        <ListGroup.Item variant="dark">
          <h5>Personal Info</h5>
        </ListGroup.Item>
        <ListGroup.Item>
          <Row>
            <Col md={{ span: 2, offset: 0 }}>
              <Badge variant="dark">Address</Badge>
            </Col>
            <Col md={{ span: 9, offset: 1 }}>
              Susukino, Aoba-ku, Yokohama-shi, Kanagawa, Japan
            </Col>
          </Row>
        </ListGroup.Item>
        <ListGroup.Item>
          <Row>
            <Col md={{ span: 2, offset: 0 }}>
              <Badge variant="dark">Phone:</Badge>
            </Col>
            <Col md={{ span: 9, offset: 1 }}>
              <Badge>+81-8087322215</Badge>
            </Col>
          </Row>
        </ListGroup.Item>
        <ListGroup.Item>
          <Row>
            <Col md={{ span: 2, offset: 0 }}>
              <Badge variant="dark">e-mail:</Badge>
            </Col>
            <Col md={{ span: 8, offset: 1 }}>
              <Badge>bharath.chakravarthi@gmail.com</Badge>
            </Col>
          </Row>
        </ListGroup.Item>
        <ListGroup.Item variant="dark">
          <h5>KeySkills</h5>
        </ListGroup.Item>
        <ListGroup.Item>
          <SkillPills />
        </ListGroup.Item>
        <ListGroup.Item variant="dark">Languages</ListGroup.Item>
        <ListGroup.Item>English</ListGroup.Item>
        <ListGroup.Item variant="dark">Contacts</ListGroup.Item>
        <ListGroup.Item>
          <a
            href="https://www.linkedin.com/in/bharath-krishna-652a7733/"
            target="_blank"
            className="m-2"
          >
            <IoLogoLinkedin size={40} />
          </a>
          <a
            href="mailto:bharath.chakravarthi@gmail.com"
            target="_blank"
            className="m-2"
          >
            <IoLogoGoogle size={40} />
          </a>
          <a
            href="https://bitbucket.org/bharath_krishna/"
            target="_blank"
            className="m-2"
          >
            <DiBitbucket size={40} />
          </a>
        </ListGroup.Item>
      </ListGroup>
    );
  }
}

export default PersonalInfo;

var skillValMap = {
  5: "Excelent",
  4: "Very Good",
  3: "Good",
  2: "Begginer",
  1: "Only Theory"
};

export class SkillPills extends Component {
  render() {
    // Skills to be retreived from api
    var skills = {
      Python: 5,
      Aiohttp: 5,
      Golang: 3,
      "Go Gin": 3,
      Microservices: 3,
      Docker: 4,
      kubermetes: 4,
      React: 2
    };

    var skillPills = [];
    Object.entries(skills).forEach(skill => {
      skillPills.push(
        <Row key={skill[0]}>
          <Col>
            <Badge pill variant="dark" className="m-1">
              {skill[0]}
            </Badge>
          </Col>
          <Col>
            <ProgressBar now={skill[1] * 20} />
            {skillValMap[skill[1]]}
          </Col>
        </Row>
      );
    });
    return skillPills;
  }
}
