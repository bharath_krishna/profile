import React, { Component } from "react";
import { Card, Row, Col, Container } from "react-bootstrap";

export default class Education extends Component {
  state = {};
  render() {
    return (
      <Container>
        <Row>
          <Col sm={2}>
            <span className="badge">2008-2010:</span>
          </Col>
          <Col sm={10}>
            <Card border="light">
              <Card.Title>Master's Degree in Bioinformatics</Card.Title>
              <Card.Subtitle>
                Kuvempu University, Karnataka, India.
              </Card.Subtitle>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col sm={2}>
            <span className="badge">2005-2008:</span>
          </Col>
          <Col sm={10}>
            <Card border="light">
              <Card.Title>Batchelor's Degree in Biotechnology</Card.Title>
              <Card.Subtitle>
                Kuvempu University, Karnataka, India.
              </Card.Subtitle>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
