import React, { Component } from "react";
import { Card, Row, Col, Container } from "react-bootstrap";

export default class Experience extends Component {
  state = {};
  render() {
    return (
      <Container>
        <Row>
          <Col sm={2}>
            <span className="badge">Sep-2014 to present:</span>
          </Col>
          <Col sm={10}>
            <Card border="light">
              <Card.Title>Application Engineer</Card.Title>
              <Card.Subtitle>Rakuten. Inc, Tokyo, Japan.</Card.Subtitle>
              <Card.Body>
                Application engineer for private cloud console, Rakuten India
                Enterprise Pvt. Ltd. Bengaluru and Rakuten, inc. Tokyo.
                <br />
                <h5>Description</h5>
                Building orchestration services for company’s private cloud
                which is a one-stop service for creating, analyzing, monitoring
                servers, includes services of DNS (Infloblox, Nominum), load
                balancing (A10, BigIP), VM operation (vSphere), Global
                authentication and authorization platform (Graph DB, OPA) etc.
                <h5>Roles and Responsibilities:</h5>
                <ul>
                  <li>
                    Research, design build asynchronous and non-blocking REST
                    APIs by consuming libraries related various services like
                    vSphere, DNS, load balancer etc.
                  </li>
                  <li>
                    Lead team of 3 members by assigning tasks providing guidance
                    on requirements.
                  </li>
                  <li>
                    Write unit and integration test cases to check the
                    integrability of services built.
                  </li>
                  <li>
                    Work as scrum master and also as individual contributor to
                    monitor the development progress and follow up with the
                    schedule.
                  </li>
                  <li>
                    Hire and train new employees required for various teams of
                    company.
                  </li>
                </ul>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col sm={2}>
            <span className="badge">Jan-2014 to Sep-2014:</span>
          </Col>
          <Col sm={10}>
            <Card border="light">
              <Card.Title>Associate IT consultant</Card.Title>
              <Card.Subtitle>
                ITC infotech and Robert Bosch, Bengaluru.
              </Card.Subtitle>
              <Card.Body>
                <h5>Roles and Responsibilities:</h5>
                <ul>
                  <li>
                    Provide support in the source code management tool (MKS, and
                    ClearQuest) hotline, automate the support task.
                  </li>
                  <li>
                    Write PERL scripts to synchronize data from and to customer
                    site.
                  </li>
                  <li>
                    Communicate with clients, prepare the test cases, perform
                    test for software migration, administrate the production and
                    proxy server.
                  </li>
                  <li>Supervise performance of the servers.</li>
                </ul>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col sm={2}>
            <span className="badge">Apr-2013 to Jan-2014:</span>
          </Col>
          <Col sm={10}>
            <Card border="light">
              <Card.Title>Software Engineer</Card.Title>
              <Card.Subtitle>eHover Systems India Pvt. Ltd.</Card.Subtitle>
              <Card.Body>
                <h5>Description</h5>
                Build cloud based personal surveillance system for working
                parents.
                <br />
                <h5>Roles and Responsibilities:</h5>
                <ul>
                  <li>
                    Development of a secure system/software to handle CCTV
                    Cameras (using EC2), to capture.
                  </li>
                  <li>
                    Store surveillance data in Amazon Cloud servers (in S3)
                    using AWS SDK, development of web interface to access
                    surveillance data using PHP and code igniter.
                  </li>
                  <li>
                    Securing data access by URL encryption, development of
                    user-friendly web interface to access surveillance data.
                  </li>
                </ul>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col sm={2}>
            <span className="badge">Apr-2011 to Apr-2013:</span>
          </Col>
          <Col sm={10}>
            <Card border="light">
              <Card.Title>Project Assistant</Card.Title>
              <Card.Subtitle>
                Dept. of P.G. Studies and Research in Biotechnology and
                Bioinformatics, Kuvempu University.
              </Card.Subtitle>
              <Card.Body>
                <h5>Project Title</h5>
                Studies on the dynamics of the Endotoxin neutralizing protein
                interaction Network (PIN) with Anti Inflammatory Molecules
                <br />
                <h5>Roles and Responsibilities:</h5>
                <ul>
                  <li>
                    Development of Perl modules/programs to perform automated
                    Molecular dynamic simulation of Endotoxin neutralizing
                    proteins.
                  </li>
                  <li>
                    Development of web interface using Perl to access these
                    simulation data. Validating these results by
                    in-vitro/in-vivo studies.
                  </li>
                  <li>
                    Development of web application to analyze SBML files using
                    python bindings of libSBML package
                  </li>
                </ul>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col sm={2}>
            <span className="badge">Sep-2010 to Feb-2011:</span>
          </Col>
          <Col sm={10}>
            <Card border="light">
              <Card.Title>Intern, Software developer</Card.Title>
              <Card.Subtitle>
                Institute of Bioinformatics and Applied Biotechnology (IBAB),
                Bengaluru.
              </Card.Subtitle>
              <Card.Body>
                <Card.Subtitle>Project Title</Card.Subtitle>
                Mammalian Gene Expression Database
                <br />
                <h5>Roles and Responsibilities:</h5>
                <ul>
                  <li>Build “Mammalian Gene Expression Database”</li>
                  <li>
                    Development web interface for maintaining bio-curator’s Data
                    records using PERL, Mysql, Java-script, and improving
                    database quality by validating curated data.
                  </li>
                </ul>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
